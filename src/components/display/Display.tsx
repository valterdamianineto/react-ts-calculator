import './Display.css';

interface Props {
    value: number | string;
}

export default (props: Props) => {
    return (
        <div className="display">
            {props.value}
        </div>
    )
}