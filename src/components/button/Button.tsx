import './Button.css'

interface Props {
    label: string | number,
    operation?: boolean,
    double?: boolean,
    triple?: boolean,
    click: (label: string | number) => void
}

export default (props: Props) => {
    let classes = 'button '
    classes += props.operation ? 'operation' : ''
    classes += props.double? 'double' : ''
    classes += props.triple? 'triple' : ''

    return (
        <button 
        onClick={e => props.click(props.label)}
            className={classes}
        >
            {props.label}
        </button>
    )
}