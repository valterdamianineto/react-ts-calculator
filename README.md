# Calculator

<p align="center" fontSize="60px">
  Calculator demo app
</p>


## Project

The project demonstrates the operation of a calculator, performing logical operations and validating the functionality of the elements.


##  Technologies

-  [JavaScript](https://www.javascript.com/)
-  [React](https://pt-br.reactjs.org/)
-  [Typescript](https://www.typescriptlang.org/)


## 📥 Installation and execution

Clone this repository.

```bash
# To run this project it is necessary to have Node installed, as well as Yarn or NPM

# Installing dependencies
$ npm i

# Running application
$ npm start

# The system default execution port is:
$ http://localhost:3000/
